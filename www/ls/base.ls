ig.fit!
if ig.containers['spotreba-recent']
  container = d3.select that
  new ig.SmallLineChart container, "spotreba"

if ig.containers['ceny-recent']
  container = d3.select that
  new ig.SmallLineChart container, "ceny"

if ig.containers['cenotvorba-marze']
  container = d3.select that
  new ig.OneBarChart container, "cenotvorbaMarze"

if ig.containers['cenotvorba-pojistovny']
  container = d3.select that
  new ig.OneBarChart container, "cenotvorbaPojistovny"

if ig.containers['ceny-all']
  container = d3.select that
  new ig.SmallLineChart container, "cenyAll"

if ig.containers['spotreba-all']
  container = d3.select that
  new ig.SmallLineChart container, "spotrebaAll"

if ig.containers['regulacni-poplatky']
  container = d3.select that
  new ig.SmallLineChart container, "regulacniPoplatky"

if ig.containers['reexport-actilyse']
  container = d3.select that
  new ig.SmallLineChart container, "reexportActilyse"

if ig.containers['distributori']
  container = d3.select that
  new ig.VerticalBarChart container, "distributori"

if ig.containers['marze-leky']
  container = d3.select that
  new ig.VerticalBarChart container, "marzeLeky"

if ig.containers['slozeni-reexport']
  container = d3.select that
  new ig.OneBarChart container, "slozeniReexport"

if ig.containers['prodeje-leky']
  container = d3.select that
  new ig.VerticalBarChart container, "prodejeLeky"

if ig.containers['doplatky']
  container = d3.select that
  new ig.VerticalBarChart container, "doplatky"

ig.watchIframes d3.selectAll "iframe[data-future-src]"

d3.selectAll "p[data-unhide]" .on \click ->
  d3.event.preventDefault!
  @className = "unhidden"
  attr = @getAttribute \data-unhide
  a = d3.selectAll "p[data-hidden='#attr']"
    .classed \unhidden yes
    .transition!
      .delay (d, i) -> 1 + i * 400
      .style \opacity 1

