headings =
  "slozeniReexport": "Náklady na léky 2012"

data =
  "cenotvorbaMarze":
    * value: 64.84
      title: """<h3>Maximální cena výrobce: <b>65 Kč</b></h3>
      <ul>
        <li>Určuje ji SÚKL ve správním řízení.</li>
        <li>Je regulována pouze u některých léků hrazených pojišťovnou.</li>
        <li>Nereguluje se, pokud je trh s danou léčivou látkou "dostatečně konkurenční"; obsahuje alespoň čtyři léky čtyř různým firem.</li>
        <li>Cena je průměrem tří nejlevnějších zemí v EU (s výjimkou zemí s jinou kupní silou, například Německa a Bulharska).</li>
        <li>U léků nehrazených pojišťovnou se nereguluje.</li>
      </ul>"""
    * value: 23.09
      title: """<h3>Marže distributora a lékárníka: <b>23 Kč</b></h3>
      <ul>
        <li>Určuje ji ministerstvo zdravotnictví v cenovém předpisu.</li>
        <li>Je regulována u všech léčivých přípravků hrazených pojišťovnou.</li>
        <li>Odvíjí se pouze od ceny výrobce; čím vyšší je cena výrobce, tím procentuálně nižší je obchodní přirážka, pohybuje se mezi 37&nbsp;%&nbsp;(u&nbsp;léků do 150&nbsp;Kč) a 4 % (u léků nad 10 000 Kč).</li>
        <li>Kromě percentuální části obsahuje ještě pevnou část: u léků do 150&nbsp;Kč je nulová, u léků nad 10 000 Kč dosahuje 758 Kč.</li>
        <li>U léků nehrazených pojišťovnou se nereguluje.</li>
      </ul>"""
    * value: 9
      title: """<h3>DPH: <b>9 Kč</b></h3>
      <ul>
        <li>Od ledna 2015 jsou léky ve snížené sazbě 10 %.</li>
      </ul>"""
  "cenotvorbaPojistovny":
    * value: 68.07
      title: """<h3>Maximální úhrada ze zdravotního pojištění: <b>68&nbsp;Kč</b></h3>
      <ul>
        <li>Určuje ji SÚKL ve správním řízení, účastníky jsou zdravotní pojišťovna a výrobce léku.</li>
        <li>Může být pro vybranou účinnou látku stanovena také na základě elektronické aukce.</li>
        <li>Pro konkrétní skupiny léků musí být vždy minimálně jeden plně hrazený pojišťovnou; o které skupiny jde, určuje zákon.</li>
        <li>U léků nehrazených pojišťovnou se nereguluje.</li>
      </ul>"""
    * value: 29.64
      title: """<h3>Maximální doplatek pacienta: <b>30 Kč</b></h3>
      <ul>
        <li>Záleží na skutečné ceně léku v konkrétní lékárně.</li>
      </ul>"""
  "slozeniReexport":
    * value: 15.6
      title: "Reexport a další vlivy<br><b>0,5 – 25,6 mld. Kč</b>"
    * value: 36.1
      title: "Pojišťovny<br><b>36 mld. Kč</b>"
    * value: 12.1
      title: "Domácnosti na předepsané<br><b>12,1 mld. Kč</b>"
    * value: 9.4
      title: "Domácnosti na volně prodejné<br><b>9,4 mld. Kč</b>"


class ig.OneBarChart
  (@parentElement, @type) ->
    @element = @parentElement.append \div
      ..attr \class \content
    if headings[@type]
      @element.append \h3
        ..html headings[@type]
    @barArea = @element.append \div
      ..attr \class \bar-area

    @data = data[@type]
    sum = d3.sum @data.map (.value)
    @y = d3.scale.linear!
      ..domain [0 sum]
      ..range [0 100]
    @barArea.append \div
      ..attr \class \items
      ..selectAll \div.item .data @data .enter!append \div
        ..attr \class \item
        ..style \height ~> "#{@y it.value}%"
    @barArea.append \div
      ..attr \class \flare

    margin = 1
    cummTop = 0
    for datum, i in @data
      datum.top = cummTop
      datum.height =
        | i == 0 or i == @data.length - 1 => @y datum.value - margin
        | otherwise                       => @y datum.value - 2 * margin
      cummTop += @y datum.value

    @dashLines = @barArea.append \div
      ..attr \class \dash-lines
      ..selectAll \div.dash-line .data @data .enter!append \div
        ..attr \class \dash-line
        ..style \top (it, i) ~>
          if i then "#{it.top + margin}%" else "0%"
        ..style \height (it, i) ~> "#{it.height}%"
    @titles = @element.append \div
      ..attr \class \titles
      ..selectAll \div.title .data @data .enter!append \div
        ..attr \class \title
        ..html -> it.title
        ..style \top (it, i) ~>
          percent =
            | i => it.top + margin
            | _ => 0
          percent += it.height / 2
          "#{percent}%"
    if @type == \slozeniReexport
      @barArea.append \div
        ..attr \class \dash-line-right
      @titles.append \div
        ..attr \class \title
        ..html "Celkové<br>náklady 2013<br><b>58,7 – 83,8 mld. Kč</b>"
