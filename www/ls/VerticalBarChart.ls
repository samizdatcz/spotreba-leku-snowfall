class ig.VerticalBarChart
  (@parentElement, @type) ->
    @data = data[@type]
    @data.sort (a, b) -> b.amount - a.amount
    for datum, index in @data
      datum.index = index
    if @type == \marzeLeky
      @fullData = @data
      @data .= slice 0, 15
    else if @type == \prodejeLeky
      @fullData = @data
      @data .= slice 0, 20
    @element = @parentElement.append \div
      ..attr \class \content
    @graphTip = new ig.GraphTip @element
    @element.append \h3
      ..html headings[@type]
    @x = d3.scale.linear!
      ..range [0 50]
    @linesElement = @element.append \div
      ..attr \class \lines

    @drawLines!
    if @type == \marzeLeky
      @setupMarzeLeky!
    else if @type == \prodejeLeky
      @setupProdejeLeky!

  drawLines: ->
    @x.domain [0 @data.0.amount]
    @lines = @linesElement.selectAll \div.line.valid .data @data, (.title)
      ..enter!append \div
        ..attr \class "line valid entering"
        ..classed \highlight -> it.highlight or -1 != it.title.indexOf '(CZ)'
        ..classed \help-available -> it.subtitle
        ..append \div
          ..attr \class \bar
        ..append \div
          ..attr \class \text
        ..append \div
          ..attr \class \amount
        ..on \mouseover (it, i) ~>
          if it.subtitle
            text = switch @type
              | \distributori => "Zaměřuje se na " + it.subtitle
              | \prodejeLeky => "Indikace: " + it.subtitle
              | otherwise              => it.subtitle
            @graphTip.display 403, 48 + it.index * 26, text
          else
            void
        ..on \mouseout ~>
          @graphTip.hide!
      ..exit!
        ..classed \valid no
        ..classed \invalid yes
        ..transition!delay 800 .remove!
      ..style \top -> "#{it.index * 26}px"
      ..select \div.bar
        ..style \width ~> "#{@x it.amount}%"
      ..select \div.text
        ..html -> "#{it.title.replace ' (CZ)' ''}"
        ..style \right ~> "#{@x it.amount}%"
      ..select \div.amount
        ..html (it, i) ~>
          if @displayed == "marzenabaleni"
            t = "#{ig.utils.formatNumber it.amount}"
            if 15 < @x it.amount then t += " Kč"
          else if @displayed == "marze"
            t = "#{ig.utils.formatNumber it.amount / 1e6}"
            if 15 < @x it.amount then t += " mil." else t += " m."
            if 20 < @x it.amount then t += " Kč"
            console.log t
          else if @type == 'doplatky'
            t = "#{ig.utils.formatNumber it.amount, 2} %"
            if i == 0 then t += " nákladů platí pacient"
          else if @type == "prodejeLeky"
            t = "#{ig.utils.formatNumber it.amount / 1e6, 1} mil."
            if it.amount > 4 * 1e6 then t += " balení"
          else
            t = "#{ig.utils.formatNumber it.amount / 1e6} mil."
            if 20 < @x it.amount then t += " Kč"
          t
        ..style \left ~> "#{100 - @x it.amount}%"
    <~ setTimeout _, 1
    @lines.classed \entering no

  setupMarzeLeky: ->
    @parentElement.classed \marze true
    @displayed = "marze"
    supp = @element.append \div .attr \class \supplement .html """
      <div class='marze'><p>
      Zobrazeny jsou kategorie léčiv, ve kterých byla nejvyšší celková
      marže, tedy součin maximální marže za balení a počtu prodaných balení.</p>
      <a href="#">
        Zobrazit podle marže na balení
      </a>
      </div>
      <div class='marzenabaleni'>
      <p>
      Zobrazeny jsou kategorie léčiv, ve kterých je nejvyšší<br>maximální marže za balení
      </p>
      <a href="#">
        Zobrazit podle celkové marže
      </a>
      </div>
      """

    supp.selectAll \a .on \click ~>
      d3.event.preventDefault!
      @toggleDisplay!

  setupProdejeLeky: ->
    @parentElement.classed \marze true
    @displayed = "baleni"
    supp = @element.append \div .attr \class \supplement .html """
      <div class='marze'>
      <a href="#">
        Zobrazit podle celkových zisků
      </a>
      </div>
      <div class='baleni'>
      <a href="#">
        Zobrazit podle prodaných balení
      </a>
      </div>
      """

    supp.selectAll \a .on \click ~>
      d3.event.preventDefault!
      @toggleDisplayProdejeLeky!


  toggleDisplay: ->
    @displayed =
      | @displayed == "marze" => "marzenabaleni"
      | otherwise             => "marze"
    @parentElement
      .classed \marze, @displayed == "marze"
      .classed \marzenabaleni, @displayed == "marzenabaleni"

    for datum in @fullData
      datum.amount = datum[@displayed]
    @fullData.sort (a, b) -> b.amount - a.amount
    @data = @fullData.slice 0, 15
    for datum, index in @data
      datum.index = index
    @drawLines!

  toggleDisplayProdejeLeky: ->
    @displayed =
      | @displayed == "baleni" => "marze"
      | otherwise             => "baleni"
    @parentElement
      .classed \marze, @displayed == "marze"
      .classed \baleni, @displayed == "baleni"

    for datum in @fullData
      datum.amount = datum[@displayed]
    @fullData.sort (a, b) -> b.amount - a.amount
    @data = @fullData.slice 0, 20
    for datum, index in @data
      datum.index = index
    @drawLines!


headings =
  distributori: "Největší výrobci léků"
  marzeLeky: "Největší marže"
  prodejeLeky: "Nejprodávanější léky"
  doplatky: "Spoluúčast pacienta v zemích OECD"

data =
  distributori: d3.tsv.parse ig.data.distributori, (row) ->
    row.amount = parseFloat row.amount
    row
  marzeLeky: d3.tsv.parse ig.data['marze-leky'], (row) ->
    row.marze = parseFloat row.marze
    row.marzenabaleni = parseFloat row.marzenabaleni
    row.amount = row.marze
    row
  prodejeLeky: d3.tsv.parse ig.data['prodeje-leky'], (row) ->
    row.baleni = parseFloat row.baleni
    row.marze = parseFloat row.marze
    row.amount = row.baleni
    row
  doplatky: d3.tsv.parse ig.data['doplatky'], (row) ->
    row.amount = parseFloat row.amount
    row
