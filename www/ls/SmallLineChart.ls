data =
  "spotreba": [[4.51 4.55 5.21 4.82 5.53 5.21 5.64 5.90 5.92 5.94 6.14]]
  "spotrebaAll": [[206344420  211168793 208575421 219890387 238873407 236739483 245375977 258197248 265258207 258674206 266485529 269129533 277951918 289812607 296536586 307335948 322334084 313524671 331623998 332496849 328389486 263184550 267280021 283290379 340502979 349562110 342926408 332267508 335799488 359364210 353879578 368864153 362179095 346249526 338954719 369149581 320090397 343206599 317706537 312204038 304588855 296690362 273945909 244721139]]
  "ceny": [[67.04 71.17 79.53 70.51 78.12 79.60 86.70 86.96 84.82 85.00 79.66]]
  "cenyAll": [[12255697231  13466137436 13547559758 14689972450 15423122915 15987937260 16873004551 17816881345 18728135268 18792207744 19239916948 19940912465 20629527752 21641848996 22172452701 23729843684 24889981974 25150894670 26258453239 28141114475 27552513669 19808646189 24888761440 32976599717 46302482901 52279532620 52936632426 51910613775 51938167221 55992374008 57059365988 62907926442 67503780923 67080007538 71215444092 79566810917 70530289907 78130198314 79602599456 86625800624 86958126994 84823472760 85003620000 79660000000]]
  "regulacniPoplatky":
    [78.2 77.96 76.37 76.99 74.88 75.14]
    [91.67 85.11 74.95 72.07 71.97 72.52]
    [void 51.88 57.93 57.85 64.42 57.87]
  "reexportActilyse":
    [804 696 791 895 1315 724 839 1027 787 963 1132 1232 1140 773 981 887 1147 1274 950 860 1121 1252 1210 1402]
    [1313 656 633 1019 1157 945 419 1128 863 2444 2366 1498 991 564 890 776 1295 913 1153 824 1414 1310 1211 948]


headings =
  "spotreba": "Spotřeba léků v definovaných denních dávkách"
  "ceny": "Náklady (v maloobchodních cenách)"
  "regulacniPoplatky": "V lékárnách platilo za recept stabilně 75 % pojištěnců"
  "cenyAll": "Náklady na léky"
  "spotrebaAll": "Spotřeba léků"
  "reexportActilyse": "Actilyse: výpadek na podzim 2013"

margins =
  "spotreba": top: 100 right: 42 bottom: 1 left: 42
  "ceny": top: 100 right: 42 bottom: 1 left: 42
  "regulacniPoplatky": top: 200 right: 35 bottom: 1 left: 0
  "cenyAll": top: 100 right: 8 bottom: 1 left: 8
  "spotrebaAll": top: 100 right: 8 bottom: 1 left: 8
  "reexportActilyse": top: 140 right: 35 bottom: 1 left: 0

class ig.SmallLineChart
  (@parentElement, @type) ->
    @element = @parentElement.append \div
      ..attr \class \content
    @element.append \h3
      ..html headings[@type]
    @markerElement = @element.append \div
      .attr \class \markers
    @fullWidth = @element.node!clientWidth
    @fullHeight = @element.node!clientHeight
    @svg = @element.append \svg
      ..attr \width @fullWidth
      ..attr \height @fullHeight
    @margin = margins[@type]
    @drawing = @svg.append \g
      ..attr \transform "translate(#{@margin.left}, #{@margin.top})"
    @width = @fullWidth - @margin.left - @margin.right
    @height = @fullHeight - @margin.top - @margin.bottom
    @data = data[@type]
    @x = d3.scale.linear!
      ..domain [0 @data.0.length - 1]
      ..range [0 @width]
    @y = d3.scale.linear!
      ..domain [0 d3.max @data.map -> d3.max it]
      ..range [@height, 0]
    @data .= map (line) ~>
      line
        .map (y, x) -> {x, y}
        .filter -> it.y != void
    line = d3.svg.line!
      ..x ~> @x it.x
      ..y ~> @y it.y
      ..interpolate "monotone"
    @drawing.append \g
      ..attr \class \lines
      ..selectAll \path .data @data .enter!append \path
        ..attr \d -> line it
    @drawing.append \g
      ..attr \class \zeroline
      ..append \line
        ..attr \class \domain
        ..attr y1: @height, y2: @height, x1:0, x2: @width
      ..selectAll \line.tick .data @data.0 .enter!append \line
        ..attr \class \tick
        ..attr \y1 @height - 2
        ..attr \y2 @height
        ..attr \x1 (d, i) ~> @x i
        ..attr \x2 (d, i) ~> @x i
    @.[type + "Annotate"]?!

  spotrebaAnnotate: ->
    @drawMarker @data[0][0], 5, -20, "rok 2003<br><b>4,51 mld. ddd</b>"
    @drawMarker @data[0][10], 5, -20, "rok 2013<br><b>6,14 mld. ddd</b>"
    @drawMarker @data[0][3], 5, 20, "rok 2006<br>Rathova reforma"
    @drawMarker @data[0][5], 5, -30, "rok 2008<br>Julínkova reforma"
    @annotateYears [2003 2013]

  cenyAnnotate: ->
    @drawMarker @data[0][0], 5, 20, "rok 2003<br><b>67,04 mld. Kč</b>"
    @drawMarker @data[0][7], 5, -20, "rok 2010<br><b>86,96 mld. Kč</b>"
    @drawMarker @data[0][10], 5, 20, "rok 2013<br><b>79,66 mld. Kč</b>"
    @annotateYears [2003 2013]

  regulacniPoplatkyAnnotate: ->
    @drawMarker {y:82, x:1.3}, 5, -40, "V roce 2009 byly od poplatků u lékaře<br>osvobozeny <b>děti do 18 let</b>"
    @drawMarker @data[0][5], 5, -20, "Poplatky za recept platí stále stejně lidí"
    @drawMarker @data[2][3], 5, 30, "Nejméně lidí platí doplatky za léky: 50 &ndash; 60 %"

    @element.append \div
      ..attr \class \percentage-notes
      ..append \div
        ..attr \class "right"
        ..style \top "#{@margin.top + @y 77}px"
        ..html "75 %"
      ..append \div
        ..attr \class "right"
        ..style \top "#{@margin.top + @y 72}px"
        ..html "73 %"
      ..append \div
        ..attr \class "right"
        ..style \top "#{@margin.top + @y 57}px"
        ..html "58 %"
      ..append \div
        ..attr \class "left"
        ..style \top "#{@margin.top + @y 96}px"
        ..html "92 %"

    @drawLegend ["Poplatky za lékařské vyšetření", "Poplatky za položku na receptu, později za recept", "Doplatky za léky"]

    @annotateYears [2008 2013]

  cenyAllAnnotate: ->
    @drawMarker @data[0][0], 5, -18, "1970: <b>12 mld. Kč</b>"
    @drawMarker @data[0][20], 5, -20, "1990: <b>28 mld. Kč</b>"
    @drawMarker @data[0][21], 5, 8, "1991: <b>20 mld. Kč</b>"
    @drawMarker @data[0][25], 5, -20, "1995<br><b>52 mld. Kč</b>"
    @drawMarker @data[0][35], 5, -20, "2005<br><b>80 mld. Kč</b>"
    @drawMarker @data[0][36], 5, 20, "2006<br><b>70 mld. Kč</b>"
    @drawMarker @data[0][40], 5, -20, "2010<br><b>87 mld. Kč</b>"
    @drawMarker @data[0][43], 5, 20, "2013<br><b>80 mld. Kč</b>"
    @annotateYears [1970 2013]

  spotrebaAllAnnotate: ->
    @drawMarker @data[0][0], 5, -40, "1970: <b>206 milionů balení</b>"
    @drawMarker @data[0][20], 5, -20, "1990: <b>328 milionů balení</b>"
    @drawMarker @data[0][21], 5, 20, "1991: <b>263 milionů balení</b>"
    @drawMarker @data[0][35], 5, -20, "2008: <b>318 milionů balení</b>"
    @drawMarker @data[0][43], 5, 20, "2013: <b>245 milionů balení</b>"
    @annotateYears [1970 2013]

  reexportActilyseAnnotate: ->
    @drawMarker {x: 9.5 y: 2412}, 5, -20, "Velký přebytek obratu v distribuci<br>nad prodejem v lékarnách koncem roku<br>2013 může značit výrazný reexport<br><small>1 132 balení v lékárnách, 2 366 v distribuci</small>"
    @drawMarker @data[0][8], 5, 17, "Běžný prodej: kolem 800 balení měsíčně"
    @annotateYears ["leden 2013" "prosinec 2014"]
    @drawLegend ["Lékárny", "Distribuční síť"]
    @element.append \div
      ..attr \class \percentage-notes
      ..append \div
        ..attr \class "right"
        ..style \top "#{@margin.top + @y 1434}px"
        ..html "1 402"
      ..append \div
        ..attr \class "right"
        ..style \top "#{@margin.top + @y 924}px"
        ..html "948"

  drawLegend: (items) ->
    @element.append \ul .attr \class \legend
      ..selectAll \li .data items .enter!append \li
        ..html -> it

  drawMarker: (dataPoint, r, distance, text) ->
    cx = @x dataPoint.x
    cy = @y dataPoint.y
    @drawing.append \circle
      ..attr \class \marker
      ..attr \cx cx
      ..attr \cy cy
      ..attr \r r
    if distance < 0
      textPositionY = "#{@fullHeight - cy - @margin.top + r + Math.abs distance}px"
      r *= -1
      textPositionAttr = "bottom"
    else
      textPositionAttr = "top"
      textPositionY = "#{cy + r + distance + @margin.top}px"

    @drawing.append \line
      ..attr \class \marker
      ..attr \x1 cx
      ..attr \y1 cy + r
      ..attr \x2 cx
      ..attr \y2 cy + r + distance
    @markerElement.append \div
      ..attr \class \marker
      ..style \left "#{cx + @margin.left}px"
      ..style textPositionAttr, textPositionY
      ..html text
    {cx, cy}

  annotateYears: (years) ->
    @element.append \div
      ..attr \class \years
      ..style \width "#{@width}px"
      ..style \left "#{@margin.left}px"
      ..style \top "#{@margin.top + @height}px"
      ..selectAll \div.year .data years .enter!append \div
        ..attr \class \year
        ..html -> it
