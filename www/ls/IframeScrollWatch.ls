recalculateOffsets = (tops, iframes) ->
  iframes.each (d, i) -> tops[i].offset = ig.utils.offset @ .top

ig.watchIframes = (iframes) ->
  tops = iframes.0.map (d, index) -> {index, offset: null}
  topsInUse = tops.slice!
  recalculateOffsets tops, iframes
  setInterval do
    -> recalculateOffsets tops, iframes
    2000
  timeout = null
  document.addEventListener "scroll", ->
    return if timeout
    timeout := setTimeout do
      ->
        timeout := null
        top = (document.body.scrollTop || document.documentElement.scrollTop) + window.innerHeight
        top = top || 0
        for index in [topsInUse.length - 1 to 0 by -1]
          topInUse = topsInUse[index]
          if topInUse.offset <= top
            topsInUse.splice index, 1
            iframe = iframes[0][topInUse.index]
            iframe.setAttribute do
              'src'
              iframe.getAttribute 'data-future-src'

      500
