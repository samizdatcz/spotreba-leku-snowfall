ig.fit = ->
  return unless $?
  $hero = $ "<div class='hero'></div>"
    ..append "<div class='overlay'></div>"
    ..append "<span class='copy'>Image &copy; Stock Exchange</span>"
    ..append "<a href='#' class='scroll-btn'>Pokračovat</a>"
    ..find 'a.scroll-btn' .bind 'click touchstart' (evt) ->
      evt.preventDefault!
      offset = $filling.offset!top + $filling.height! - 80
      d3.transition!
        .duration 800
        .tween "scroll" scrollTween offset
  $ 'body' .prepend $hero

  $ '#article h1' .html 'Léky v datech:<br>Na čem jsme závislí,<br>kolik utrácíme?'

  $filling = $ "<div class='ig filling'></div>"
    ..css \height $hero.height! + 80
  $ "p.perex" .after $filling
  $ window .bind \resize ->
    $filling.css \height $hero.height! + 80
  <~ $
  $ '#aside' .remove!

scrollTween = (offset) ->
  ->
    interpolate = d3.interpolateNumber do
      window.pageYOffset || document.documentElement.scrollTop
      offset
    (progress) -> window.scrollTo 0, interpolate progress
